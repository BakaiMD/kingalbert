//������ ����� ����������� � ���������, ������ ����� ���������� ������� �� 4 ����, ������� ����� �� �����, ������ �� ������ �����
//����� ��� ����� 3 ������ � ���� ������� �����(�����), �����  ����� ������� �������, ��� ��������� ������ ������� ������
//� ������ �������� ����� ����� ���������� � 4 ����, �.� ������ ������ 16 000$


#ifndef __SECOND_ROUNDJM_H__
#define __SECOND_ROUNDJM_H__
#include "cocos2d.h"
#include "PlayingCards.h"
#include "HelloWorldScene.h"

USING_NS_CC;

typedef vector<CardSprite> VectorOfCardSprites;

class SecondRoundJM : public cocos2d::CCLayerColor
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	PlayingCards generateItemsOfRound();

	// implement the "static create()" method manually
	CREATE_FUNC(SecondRoundJM);



};

#endif 

