#include "HelloWorldScene.h"
#include "PlayingCards.h"
#include<iostream>
#include <string>
#include <sstream>
#include <SecondRoundJM.h>
USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}




bool HelloWorld::init()
{
	if (!CCLayerColor::initWithColor(ccc4(0, 255, 0, 50))) {
		return false;
	}
	PlayingCards deck(52);//������ ����� : �������� ������ ��� ������� ����� , ������������� ������ ����� ������
	CardSprite ItemOfGame; // ����� ���������� ���� �����(����� � ���� , ��� ��������) � ������
		ItemOfGame.CardFromDeck = deck.createPictName(0);//������� ��� ����� 
	auto player = Sprite::create(ItemOfGame.CardFromDeck.name);// ������� ������
		player->setAnchorPoint(cocos2d::Vec2(0.0, 0.5));
		player->setPosition(300, 300);
		//ItemOfGame.sprt = player;// ��������� � ����� �ardSprite
		this->addChild(player);
		ItemOfGame.SPR = player;
		
		Label* score = Label::createWithSystemFont("Your score: 1000$", "Arial", 24);
		score->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));// ������ ������ �� ������ ������ ����
		score->setPosition(10, 10);
		this->addChild(score);
		Label* red = Label::createWithSystemFont("RED", "Arial", 24);// �������� ������ ����� , ������� ��� �������� ����� �����
		Label* black = Label::createWithSystemFont("BLACK", "Arial", 24);
		
		
		red->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));
		red->setPosition(250, 300);
		black->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));
		black->setPosition(350, 300);
		this->addChild(red);
		this->addChild(black);
		Label* sprite1;
		Label* sprite2;// ���������� ������ � �������� �����
		if (ItemOfGame.CardFromDeck.suit < 2)
		{
			sprite1 = red;
			sprite2 = black;
		}
		else
		{
			sprite1 = black;
			sprite2 = red;
		}

		
	/*	auto touchListener = EventListenerTouchOneByOne::create();
		
		touchListener->onTouchBegan = CC_CALLBACK_2(HelloWorld::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(HelloWorld::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(HelloWorld::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(HelloWorld::onTouchCancelled, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

*/
		auto touchListener1 = EventListenerTouchOneByOne::create();
		auto touchListener2 = EventListenerTouchOneByOne::create();//������ ������� ��� ����������� ���� �� ������ �������� 
		touchListener1->onTouchBegan = [](Touch* touch, Event* event) -> bool {// ���� ����� ������ ���������� ���� , ������ ���� ����� ������������� � 4����

			auto bounds = event->getCurrentTarget()->getBoundingBox();

			if (bounds.containsPoint(touch->getLocation())) {
				std::stringstream touchDetails;
				touchDetails << "YOU'VE WON  ";

				MessageBox(touchDetails.str().c_str(), "Touched");
				auto scene2 = SecondRoundJM::createScene();
				Director::getInstance()->replaceScene(scene2);
			}
			
			return true;
		};
		touchListener2->onTouchBegan = [](Touch* touch, Event* event) -> bool {// � ��������� ������ �������� � ������ ������� ������

			auto bounds = event->getCurrentTarget()->getBoundingBox();

			if (bounds.containsPoint(touch->getLocation())) {
				
				std::stringstream touchDetails;
				touchDetails << "YOU'VE LOST  ";

				MessageBox(touchDetails.str().c_str(), "Touched");
			}
			return true;
		};

		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener1, sprite1);
		Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(touchListener2, sprite2);
		
		return true;
}


//bool HelloWorld:: onTouchBegan(cocos2d::Touch* touch, cocos2d::Event*)
//{
//	if (touch->getLocation() == red->getPosition())
//	{
//		auto click = Label::createWithSystemFont("YES", "Arial", 24);
//		click->setPosition(100,100);
//		this->addChild(click);
//	}
//	else
//	{
//		auto click = Label::createWithSystemFont("NO", "Arial", 24);
//		click->setPosition(touch->getLocation());
//		this->addChild(click);
//	}
//	return true;
//}
//
//void HelloWorld::onTouchEnded(Touch* touch, Event* event)
//{
//	cocos2d::log("touch ended");
//}
//
//void HelloWorld::onTouchMoved(Touch* touch, Event* event)
//{
//	cocos2d::log("touch moved");
//}
//
//void HelloWorld::onTouchCancelled(Touch* touch, Event* event)
//{
//	cocos2d::log("touch cancelled");
//}