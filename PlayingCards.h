#ifndef PLAYING_CARDS_H
#define PLAYING_CARDS_H
#include<iostream>
#include <string>

using namespace std;


struct Card
{
	int suit, rang;
	Card() :suit(-1), rang(-1) {};
	Card::Card(int s, int r)
	{
		if (s < 0 || s>3)
			throw "Illegal suit";
		if (r < 0 || r>12)
			throw "Illegal rang";
		suit = s;
		rang = r;
	}
};


struct CardName
{
	string name;// �������� ����������� � ��������
	int rang;
	int suit;
};

class PlayingCards
{
public:
	PlayingCards();
	PlayingCards(int);
	PlayingCards(const PlayingCards &obj);
	PlayingCards(PlayingCards &&obj);
	PlayingCards(const Card&);
	~PlayingCards();
	CardName createPictName(int number);
	int getn() const;
	Card getFirstCard()const;
	int operator ()(int) const;
	int operator [] (int) const;
	PlayingCards& regulatPack();
	PlayingCards allotSub(int mas) const;
	PlayingCards & addCard(const Card &card);


private:
	static const int SZ = 52;
	int n;
	Card *pack = nullptr;
	int findTheSame(int rang, int suit);
};

#endif //MY_SYMBOL_H