#include "HelloWorldScene.h"
#include "PlayingCards.h"
#include "SecondRoundJM.h"
#include<iostream>
#include <string>
#include <sstream>
#include <time.h>

USING_NS_CC;

Scene* SecondRoundJM::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = SecondRoundJM::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}




bool SecondRoundJM ::init()
{
	if (!CCLayerColor::initWithColor(ccc4(0, 255, 0, 50))) {
		return false;
	}
	// ����� ������������ ����� (�������� , ������� ) �� ����� ����


	PlayingCards deck = generateItemsOfRound();;//������ ����� 
	VectorOfCardSprites ItemsOfRound;//������ ����( ������ ������� ������� ������ �������� �����(��� ���� � ��������) � ����-����� �����
	for (int i = 0; i < 4; ++i)
	{
		CardSprite Item;
		Item.CardFromDeck = deck.createPictName(i);//������� ��� ����� 
		auto player = Sprite::create(Item.CardFromDeck.name);// ������� ������
		player->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));
		player->setPosition(100+100*i, 300);
		this->addChild(player);
		Item.SPR = player;
		ItemsOfRound.push_back(Item);
	}
	Label* score = Label::createWithSystemFont("Your score: 4000$", "Arial", 24);
	score->setAnchorPoint(cocos2d::Vec2(0.0, 0.0));// ������ ������ �� ������ ������ ����
	score->setPosition(10, 10);
	this->addChild(score);
	
	
	/// ����� ��� ������� ������ ���� (������� ����� � ������� ������ �� 4 ���� (����� ��� ��� ������ � ���� �������)
	
	// ���������� ������ � �������� �����
	int rightSuit = 0;// ������ ���������� � ����� ����� ��������� ������� �����
	for (; rightSuit < 4; ++rightSuit)
	{
		if (ItemsOfRound.at(rightSuit).CardFromDeck.suit < 2)
		{
			break;
		}
	}

	EventListenerTouchOneByOne* _touchListeners[4];
	for (int i = 0; i < 4;++i)
	_touchListeners[i]= EventListenerTouchOneByOne::create();

	for (int i = 0; i < 4; ++i)
	{
		if (i != rightSuit) {
			_touchListeners[i]->onTouchBegan = [](Touch* touch, Event* event) -> bool {// ���� ����� ������ ���������� ���� , ������ ���� ����� ������������� � 4����

				auto bounds = event->getCurrentTarget()->getBoundingBox();

				if (bounds.containsPoint(touch->getLocation())) {
					std::stringstream touchDetails;
					touchDetails << "YOU'VE LOSEN  ";

					MessageBox(touchDetails.str().c_str(), "Touched");
				}

				return true;
			};
		}
		else
		{
			_touchListeners[i]->onTouchBegan = [](Touch* touch, Event* event) -> bool {// � ��������� ������ �������� � ������ ������� ������

				auto bounds = event->getCurrentTarget()->getBoundingBox();

				if (bounds.containsPoint(touch->getLocation())) {

					std::stringstream touchDetails;
					touchDetails << "YOU'VE WON  ";

					MessageBox(touchDetails.str().c_str(), "Touched");
				}
				return true;
			};
		}
	}
	for (int i = 0; i < 4;++i)
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchListeners[i],ItemsOfRound.at(i).SPR);

	return true;
}


PlayingCards SecondRoundJM::generateItemsOfRound()
{
	srand(time(0));
	PlayingCards deck;
	for (int i = 0; i<3; i++) {
		Card black;
		int suit = rand() % 2 +2;
		int rang = rand() % 13;
			black.suit = suit;
			black.rang = rang;
			deck.addCard(black);
	}
	Card red;
	int suit = rand() % 2;
	int rang = rand() % 13;
	red.suit = suit;
	red.rang = rang;
	deck.addCard(red);
	return deck;
}