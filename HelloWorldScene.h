#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "PlayingCards.h"

USING_NS_CC;
class CardSprite
{
public:
	CardName CardFromDeck;
	cocos2d::Sprite* SPR;
};
class HelloWorld : public cocos2d::CCLayerColor
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
	/*virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);*/
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);



};

#endif // __HELLOWORLD_SCENE_H__

